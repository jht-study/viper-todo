//
//  ToDoStore.swift
//  ToDo
//
//  Created by Administrador on 14/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation
import CoreData

class ToDoStore: NSObject {
    var persistentStoreCoordinator: NSPersistentStoreCoordinator!
    var managedObjectModel: NSManagedObjectModel!
    var managedObjectContext: NSManagedObjectContext!
    
    override init() {
        managedObjectModel = NSManagedObjectModel.mergedModel(from: nil)
        
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        let domain = FileManager.SearchPathDomainMask.userDomainMask
        let directory = FileManager.SearchPathDirectory.documentDirectory
        
        let applicationDocumentsDirectory = FileManager.default.urls(for: directory, in: domain).first!
        let options = [NSMigratePersistentStoresAutomaticallyOption: true,
                       NSInferMappingModelAutomaticallyOption: true]
        
        let storeURL = applicationDocumentsDirectory.appendingPathComponent("ToDo.sqlite")
        
        try! persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: "", at: storeURL, options: options)
        
        managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        managedObjectContext.undoManager = nil
        
        super.init()
    }
    
    func fetchEntriesWithPredicate(_ predicate: NSPredicate, sortDescriptors: [NSSortDescriptor], completionBlock: (([ManagedTodoItem]) -> Void)!){
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "TodoItem")
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = sortDescriptors
        
        managedObjectContext.perform {
            let queryResult = try? self.managedObjectContext.fetch(fetchRequest)
            let managedResults = queryResult! as! [ManagedTodoItem]
            completionBlock(managedResults)
        }
    }
}
