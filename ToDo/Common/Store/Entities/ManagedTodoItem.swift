//
//  ManagedTodoItem.swift
//  ToDo
//
//  Created by Administrador on 14/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation
import CoreData

class ManagedTodoItem: NSManagedObject {
    @NSManaged var name: String
    @NSManaged var date: Date
}
