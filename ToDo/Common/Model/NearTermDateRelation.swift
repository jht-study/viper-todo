//
//  NearTermDateRelation.swift
//  ToDo
//
//  Created by Administrador on 14/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

enum NearTermDateRelation{
    case outOfRange, today, tomorrow, laterThisWeek, nextWeek
}
