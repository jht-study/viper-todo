//
//  TodoItem.swift
//  ToDo
//
//  Created by Administrador on 14/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

struct TodoItem {
    let dueDate: Date
    let name: String
}
