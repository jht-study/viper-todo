//
//  RootWireFrame.swift
//  ToDo
//
//  Created by Administrador on 15/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import UIKit

class RootWireFrame: NSObject {
    func showRootViewController(_ viewController: UIViewController, inWindow: UIWindow) {
        let navigationController = navigationControllerFromWindow(inWindow)
        navigationController.viewControllers = [viewController]
    }
    
    func navigationControllerFromWindow(_ window: UIWindow) -> UINavigationController {
        let navigationController = window.rootViewController as! UINavigationController
        return navigationController
    }
}
