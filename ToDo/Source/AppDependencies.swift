//
//  AppDependencies.swift
//  ToDo
//
//  Created by Administrador on 17/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation
import UIKit

class AppDependencies {
    var listWireframe = ListWireframe()
    
    init() {
        configureDependencies()
    }
    
    func installRootViewControllerIntoWindow(_ window: UIWindow) {
        listWireframe.presentListInterfaceFromWindow(window)
    }
    
    func configureDependencies() {
        let coreDataStore = ToDoStore()
        let roorWireframe = RootWireFrame()
        
        let listPresenter = ListPresenter()
        
        let listDataManager = ListDataManager()
        listDataManager.toDoStore = coreDataStore
        
        let listInteractor = ListInteractor(dataManager: listDataManager)
        listInteractor.output = listPresenter
        
        listPresenter.listInteractor = listInteractor
        listPresenter.listWireframe = listWireframe
        
        listWireframe.listPresenter = listPresenter
        listWireframe.rootWireframe = roorWireframe
    }
}
