//
//  ListDataManager.swift
//  ToDo
//
//  Created by Administrador on 15/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

class ListDataManager: NSObject {
    var toDoStore: ToDoStore?
    
    func todoItemsBetweenStartDate(_ startDate: Date, endDate: Date, completion: (([TodoItem]) -> Void)!) {
        let calendar = Calendar.autoupdatingCurrent
        let beginnig = calendar.dateForBeginningOfDay(startDate)
        let end = calendar.dateForEndOfDay(endDate)
        
        let predicate = NSPredicate(format: "(date >= %@) AND (date <= %@)", beginnig as CVarArg, end as CVarArg)
        let sortDescriptors: [NSSortDescriptor] = []
        
        toDoStore?.fetchEntriesWithPredicate(predicate,
          sortDescriptors: sortDescriptors,
          completionBlock: { entries in
            let todoItems = self.todoItemsFromDataStoreEntries(entries)
            completion(todoItems)
        })
    }
    
    func todoItemsFromDataStoreEntries(_ entries: [ManagedTodoItem]) -> [TodoItem] {
        let todoItems: [TodoItem] = entries.map {entry in
            TodoItem(dueDate: entry.date, name: entry.name as String)
        }
        return todoItems
    }
}
