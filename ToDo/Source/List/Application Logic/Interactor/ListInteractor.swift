//
//  ListInteractor.swift
//  ToDo
//
//  Created by Administrador on 15/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

class ListInteractor: NSObject, ListInteractorInput {
    var output: ListInteractorOutput?
    
    //let clock: Clock
    let dataManager: ListDataManager
    
    init(dataManager: ListDataManager) {
        self.dataManager = dataManager
    }
    
    func findUpcomingItems() {
        let today = Date()
        let endOfNextWeek = Calendar.current.dateForEndOfFollowingWeekWithDate(today)
        
        dataManager.todoItemsBetweenStartDate(today,
                                            endDate: endOfNextWeek,
                                            completion: { todoItems in
                                                let upcommingItem = self.uncomingItemsFromToDoItems(todoItems)
                                                self.output?.foundUpcomingItems(upcommingItem)
                                                    
        })
    }
    
    func uncomingItemsFromToDoItems(_ todoItems: [TodoItem]) -> [UpcomingItem] {
        let calendar = Calendar.autoupdatingCurrent
        
        let upcomingItems: [UpcomingItem] = todoItems.map { todoItem in
            let dateRelation = calendar.nearTermRelationForDate(todoItem.dueDate, relativeToToday: Date())
            return UpcomingItem(title: todoItem.name, dueDate: todoItem.dueDate, dateRelation: dateRelation)
        }
        return upcomingItems
    }
 }
