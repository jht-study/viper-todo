//
//  UpcomingItem.swift
//  ToDo
//
//  Created by Administrador on 15/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

struct UpcomingItem: Equatable {
    let title: String
    let dueDate: Date
    let dateRelation: NearTermDateRelation
    
    init(title: String, dueDate: Date = Date(), dateRelation: NearTermDateRelation = NearTermDateRelation.outOfRange) {
        self.title = title
        self.dueDate = dueDate
        self.dateRelation = dateRelation
    }
}

extension UpcomingItem {
    static func == (leftSide: UpcomingItem, rightSide: UpcomingItem) -> Bool {
        var hasEqualSections = false
        
        hasEqualSections = rightSide.title == leftSide.title
        if hasEqualSections == false {
            return false
        }
        
        hasEqualSections = rightSide.dueDate == rightSide.dueDate
        if hasEqualSections == false {
            return false
        }
        
        hasEqualSections = rightSide.dateRelation == rightSide.dateRelation
        
        return hasEqualSections
    }
}
