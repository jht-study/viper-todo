//
//  ListInteractorIO.swift
//  ToDo
//
//  Created by Administrador on 15/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

protocol ListInteractorInput {
    func findUpcomingItems()
}

protocol ListInteractorOutput {
    func foundUpcomingItems(_ upcomingItems: [UpcomingItem])
}
