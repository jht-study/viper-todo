//
//  ListViewInterface.swift
//  ToDo
//
//  Created by Administrador on 17/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

protocol  ListViewInterface {
    func showNoContentMessage()
    func showUpcomingDisplayData(_ data: UpcomingDisplayData)
    func reloadEntries()
}
