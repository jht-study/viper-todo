//
//  ListVC.swift
//  ToDo
//
//  Created by Administrador on 17/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation
import UIKit

var ListEntryCellIdentifier = "ListEntryCell"

class ListVC: UITableViewController, ListViewInterface {
    var eventHandler: ListModuleInterface?
    var dataProperty: UpcomingDisplayData?
    var strongTableView: UITableView?
    
    @IBOutlet var noContentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        strongTableView = tableView
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        eventHandler?.updateView()
    }
    
    func configureView() {
        navigationItem.title = "VIPER ToDo"
        
        let addItem = UIBarButtonItem(barButtonSystemItem: .add
            , target: self, action: #selector(ListVC.didTapAddButton))
        
        navigationItem.rightBarButtonItem = addItem
    }
    
    @objc func didTapAddButton() {
        eventHandler?.addNewEntry()
    }
    
    // MARK: - ListViewInterface
    func showNoContentMessage() {
        view = noContentView
    }
    
    func showUpcomingDisplayData(_ data: UpcomingDisplayData)
    {
        view = strongTableView
        dataProperty = data
        
        reloadEntries()
    }
    
    func reloadEntries() {
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return dataProperty?.sections.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let upcomingSection = dataProperty?.sections[section]
        return upcomingSection!.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListEntryCellIdentifier, for: indexPath)
        let upcomingSection = dataProperty?.sections[(indexPath as NSIndexPath).section]
        let upcomingItem = upcomingSection!.items[(indexPath as NSIndexPath).row]
        
        cell.textLabel?.text = upcomingItem.title
        cell.detailTextLabel?.text = upcomingItem.dueDate
        cell.imageView?.image = UIImage(named: upcomingSection!.imageName)
        cell.selectionStyle = .none
        
        return cell
    }
}
