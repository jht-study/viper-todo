//
//  ListWireframe.swift
//  ToDo
//
//  Created by Administrador on 17/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation
import UIKit

let ListViewControllerIdentifier = "ListViewController"

class ListWireframe: NSObject {
    var listPresenter: ListPresenter?
    var rootWireframe: RootWireFrame?
    var listViewController: ListVC?
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyboard
    }
    
    func listViewControllerFromStoryBoard() -> ListVC {
        let storyboard = mainStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: ListViewControllerIdentifier) as! ListVC
        return viewController
    }
    
    func presentListInterfaceFromWindow(_ window: UIWindow) {
        let viewController = listViewControllerFromStoryBoard()
        viewController.eventHandler = listPresenter
        listViewController = viewController
        listPresenter?.userInterface = viewController
        rootWireframe?.showRootViewController(viewController, inWindow: window)
    }
}
