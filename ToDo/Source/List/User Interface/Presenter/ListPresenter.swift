//
//  ListPresenter.swift
//  ToDo
//
//  Created by Administrador on 17/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

class ListPresenter: NSObject, ListInteractorOutput, ListModuleInterface {
    var listInteractor : ListInteractorInput?
    var listWireframe: ListWireframe?
    var userInterface : ListViewInterface?
    
    // MARK: ListInteractorOutput
    
    func foundUpcomingItems(_ upcomingItems: [UpcomingItem]) {
        if upcomingItems.count == 0 {
            userInterface?.showNoContentMessage()
        } else {
            
        }
    }
    
    func updateUserInterfaceWithUpcomingItems(_ upcomingItems: [UpcomingItem]) {
        let upcomingDisplayData = upcomingDisplayDataWithItems(upcomingItems)
        userInterface?.showUpcomingDisplayData(upcomingDisplayData)
    }
    
    func upcomingDisplayDataWithItems(_ upcomingItems: [UpcomingItem]) -> UpcomingDisplayData {
        let collection = UpcomingDisplayDataCollection()
        collection.addUpcomingItems(upcomingItems)
        return collection.collectedDisplayData()
    }
    
    // MARK: ListModuleInterface
    
    func addNewEntry() {
        
    }
    
    func updateView() {
        listInteractor?.findUpcomingItems()
    }
}
