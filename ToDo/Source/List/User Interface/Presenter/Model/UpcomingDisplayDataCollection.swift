//
//  UpcomingDisplayDataCollection.swift
//  ToDo
//
//  Created by Administrador on 17/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

class UpcomingDisplayDataCollection {
    let dayFormatter = DateFormatter()
    var sections: [NearTermDateRelation: [UpcomingDisplayItem]] = [:]
    
    init() {
        dayFormatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "EEEE", options: 0, locale: .autoupdatingCurrent)
    }
    
    func formattedDay(_ date: Date, dateRelation: NearTermDateRelation) -> String {
        return dateRelation == NearTermDateRelation.today ? "": dayFormatter.string(from: date)
    }
    
    func displayItemForUpcomingItem(_ upcomingItem: UpcomingItem) -> UpcomingDisplayItem {
        let day = formattedDay(upcomingItem.dueDate as Date,
                               dateRelation: upcomingItem.dateRelation)
        let displayItem = UpcomingDisplayItem(title: upcomingItem.title,
                                              dueDate: day)
        return displayItem
        
    }
    
    func addDisplayItem(_ displayItem: UpcomingDisplayItem, dateRelation: NearTermDateRelation) {
        if var realSection: [UpcomingDisplayItem] = sections[dateRelation] {
            realSection.append(displayItem)
            sections[dateRelation] = realSection
        } else {
            var newSection: [UpcomingDisplayItem] = []
            newSection.append(displayItem)
            sections[dateRelation] = newSection
        }
    }
    
    func addUpcomingItem(_ upcomingItem: UpcomingItem) {
        let displayItem = displayItemForUpcomingItem(upcomingItem)
        addDisplayItem(displayItem, dateRelation: upcomingItem.dateRelation)
    }
    
    func addUpcomingItems(_ upcomingItems: [UpcomingItem])
    {
        for upcomingItem in upcomingItems {
            addUpcomingItem(upcomingItem)
        }
    }
    
    func sortedNearTermDateRelations() -> [NearTermDateRelation] {
        return [.today, .tomorrow, .laterThisWeek, .nextWeek]
    }
    
    func sectionTitleForDateRelation(_ dateRelarion: NearTermDateRelation) -> String {
        switch dateRelarion {
        case .today:
            return "Today"
        case .tomorrow:
            return "Tomorrow"
        case .laterThisWeek:
            return "Later This Week"
        case .nextWeek:
            return "Next Week"
        case .outOfRange:
            return "Unknown"
        }
    }
    
    func sectionImageNameForDateRelation(_ dateRelarion: NearTermDateRelation) -> String {
        switch dateRelarion {
        case .today:
            return "check"
        case .tomorrow:
            return "alarm"
        case .laterThisWeek:
            return "circle"
        case .nextWeek:
            return "calendar"
        case .outOfRange:
            return "paper"
        }
    }
    
    
    func displaySectionForDateRelation(_ dateRelation: NearTermDateRelation) -> UpcomingDisplaySection {
        let sectionTitle = sectionTitleForDateRelation(dateRelation)
        let imageName = sectionImageNameForDateRelation(dateRelation)
        let items: [UpcomingDisplayItem] = sections[dateRelation]!
        
        return UpcomingDisplaySection(name: sectionTitle, imageName: imageName, items: items)
    }
    
    func sortedUpcomingDisplaySections() -> [UpcomingDisplaySection] {
        let keys = sortedNearTermDateRelations()
        var displaySections: [UpcomingDisplaySection] = []
        
        for dateRelation in keys {
            let itemArray = sections[dateRelation]
            
            if itemArray != nil  {
                let displaySection = displaySectionForDateRelation(dateRelation)
                displaySections.insert(displaySection, at: displaySections.endIndex)
            }
        }
        return displaySections
    }
    
    func collectedDisplayData() -> UpcomingDisplayData {
        let collectedSections: [UpcomingDisplaySection] = sortedUpcomingDisplaySections()
        return UpcomingDisplayData(sections: collectedSections)
    }
}
