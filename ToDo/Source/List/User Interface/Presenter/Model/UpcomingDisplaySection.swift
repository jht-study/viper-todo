//
//  UpcomingDisplaySection.swift
//  ToDo
//
//  Created by Administrador on 17/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

struct UpcomingDisplaySection {
    let name: String
    let imageName: String
    var items: [UpcomingDisplayItem] = []
}

extension UpcomingDisplaySection: Equatable {
    static func == (leftSide: UpcomingDisplaySection, rigthSide: UpcomingDisplaySection) -> Bool {
        return rigthSide.items == leftSide.items
    }
}
