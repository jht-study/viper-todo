//
//  UpdateDisplayData.swift
//  ToDo
//
//  Created by Administrador on 17/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

struct UpcomingDisplayData {
    let sections: [UpcomingDisplaySection]
}

extension UpcomingDisplayData: Equatable {
    static func == (leftSide: UpcomingDisplayData, rigthSide: UpcomingDisplayData) -> Bool {
        return rigthSide.sections == leftSide.sections
    }
}
