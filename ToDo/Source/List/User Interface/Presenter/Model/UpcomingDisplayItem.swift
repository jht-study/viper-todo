//
//  UpcomingDisplayItem.swift
//  ToDo
//
//  Created by Administrador on 17/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

struct UpcomingDisplayItem {
    let title: String
    let dueDate: String
}

extension UpcomingDisplayItem: Equatable {
    static func == (leftSide: UpcomingDisplayItem, rightSide: UpcomingDisplayItem) -> Bool {
        return rightSide.title == leftSide.title && rightSide.dueDate == leftSide.dueDate
    }
}

extension UpcomingDisplayItem: CustomStringConvertible {
    var description: String {
        return "\(title) -- \(dueDate)"
    }
}
